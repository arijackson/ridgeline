# -*- coding: utf-8 -*-
"""
thingspeak data api -> raw

map thingspeak ids to channel ids, several function for requesting
data from the thingspeak API
"""
import pandas as pd
import pickle
import datetime as dt
import pytz
import requests
import os
from dotenv import load_dotenv, find_dotenv

load_dotenv(find_dotenv(usecwd=True))
root = os.environ.get('ROOT')
ts_api_key = os.environ.get('TS_API_KEY')


def get_channel_tag_id_dict():
    """
    pull channel ids in bulk and store, channel ids are needed to request
    data from thingspeak API and are mapped to thingspeak ids but cannot
    be requested using the thingspeak id and must be compiled in bulk
    """
    # request channel list from thingspeak
    url = 'https://api.thingspeak.com/channels.json?api_key=' + ts_api_key
    r = requests.get(url)
    channel_list = r.json()
    # to help avoid hitting channel limits in ThingSpeak, print the
    # number of channels currently hosted to ThingSpeak
    print('')
    print(
        '    Currently ' + str(len(channel_list)) + ' channels are hosted' +
        ' on ThingSpeak.')
    print('')
    # iterate over channels and create df of channel ids and tags
    df = pd.DataFrame(columns=['id', 'tag', 'created_at'])
    for channel in channel_list:
        channel_id = str(channel['id'])
        channel_tag = channel['tags'][0]['name'].upper()
        channel_created_at = pd.to_datetime(channel['created_at'])
        vals = [channel_id, channel_tag, channel_created_at]
        df = df.append(pd.DataFrame(columns=df.columns, data=[vals]))
    df.to_csv(root + '/data/raw/thingspeak/channels.csv', index=False)
    # if a tag is duplicated, keep teh most recently created channel
    df.sort_values(by=['tag', 'created_at'], ascending=False, inplace=True)
    df.drop_duplicates(subset=['tag'], keep='first', inplace=True)
    channel_tag_id_dict = df.set_index('tag')['id'].to_dict()
    # store map for requesting data on a channel by channel basis
    pickle.dump(
        channel_tag_id_dict,
        open(root + '/data/raw/thingspeak/channel_tag_id_dict.p', 'wb'))


def get_channel_data(thingspeak_tag):
    """
    to request timeseries data, the ThingSpeak API requires a channel ID,
    channel read key, and field number. channel ID and read key are requested
    here
    """
    # get channel id using thingspeak id
    channel_tag_id_dict = pickle.load(
        open(root + '/data/raw/thingspeak/channel_tag_id_dict.p', 'rb'))
    channel_id = channel_tag_id_dict[thingspeak_tag]
    # request channel info to get read key
    url = (
        'https://api.thingspeak.com/channels/' +
        channel_id + '.json?api_key=' + ts_api_key)
    r = requests.get(url)
    channel = r.json()
    channel_api_keys = [x['api_key'] for x in channel['api_keys']]
    channel_read_key = channel_api_keys[1]
    # return channel id and read key so timeseries data can be requested
    return (channel_id, channel_read_key)


def get_field_id(channel_id, channel_read_key, input_label):
    """
    in thingspeak a channel may contain several fields, to request a specific
    time series of data we need the field id (field1, field2, etc), field ids
    are mapped field descriptions ('Pulse1 Count', 'Voltage1 V', ect), however
    these descriptions are inconsistent (for example 'P1 counts' and
    'Pulse1 Count' both describe pulse input 1). In Fulcrum, sensor inputs are
    labeled either P or T pluse the number that appears on the circuit board
    (P1, T3, etc). Voltages are always V, signal strength is always Q, and
    relative humidity is always RH. A map has been manually created between
    the descriptions in thingspeak and the input labels in fulcrum that is used
    to identify the field id needed to request data from thingspeak
    """
    # a manually created map between field labels in fulcrum and
    # field descriptions in thingspeak is stored in data/raw/project
    field_description_fulcrum_label_dict = pd.read_csv(
        root + '/data/raw/thingspeak/field_descriptions.csv',
        dtype=str, index_col='key')['value'].to_dict()
    # request a short amount of timeseries data from the channel to get
    # a map between field descriptions and field ids
    timezone = 'America/New_York'
    start = (dt.datetime.now(
        tz=pytz.timezone(timezone)) - dt.timedelta(seconds=60)).strftime(
            format='%Y-%m-%d %H:%M:%S')
    end = dt.datetime.now(
        tz=pytz.timezone(timezone)).strftime(format='%Y-%m-%d %H:%M:%S')
    url = (
        'https://api.thingspeak.com/channels/' + channel_id +
        '/feeds.json?api_key=' + channel_read_key +
        '&timezone=' + timezone +
        '&start=' + start +
        '&end=' + end +
        '&metadata=true')
    r = requests.get(url)
    field_id_field_description_dict = dict(
        filter(lambda x: 'field' in x[0], r.json()['channel'].items()))
    # an encoding error (\272) requires spliting temperature descriptions
    # that can't represent the degree symbol for deg F
    for key, value in field_id_field_description_dict.items():
        if 'Temperature' in value:
            field_id_field_description_dict[key] = value.split(' ')[0]
    # reassign the thingspeak descriptions as fulcrum labels
    field_id_fulcrum_label_dict = dict()
    for key, value in field_id_field_description_dict.items():
        field_id_fulcrum_label_dict[key] = (
            field_description_fulcrum_label_dict[value])
    # invert the map so that the field id is returned given teh label
    # from fulcrum
    fulcrum_label_field_id_dict = {
        v: k for k, v in field_id_fulcrum_label_dict.items()}
    field_id = fulcrum_label_field_id_dict[input_label]
    return field_id


def get_field_timeseries_data(
        channel_id, channel_read_key, field_id,
        install_date, removal_date):
    """
    request data for specific device/channel/field/point in time,
    data requested iteratively in 4000 minute intervals because of
    limitation of thingspeak
    """
    # parse field id to get field number
    field_number = field_id.split('field')[1]
    # convert date objects to date time objects
    timezone = 'America/New_York'
    start = pd.to_datetime(
        str(install_date.year) + '-' +
        str(install_date.month) + '-' +
        str(install_date.day)).tz_localize(
            tz=timezone)
    end = pd.to_datetime(
        str(removal_date.year) + '-' +
        str(removal_date.month) + '-' +
        str(removal_date.day)).tz_localize(
            tz=timezone) + dt.timedelta(days=1)
    start = start - dt.timedelta(minutes=4000)
    middle = end - dt.timedelta(minutes=4000)
    # request timeseries data in 4000 minute intervals (max available from
    # thingspeak in one request) and append to list for concatenation
    df_list = list()
    while start < middle:
        url = (
            'https://api.thingspeak.com/' +
            'channels/' + channel_id +
            '/fields/' + field_number +
            '.json?api_key=' + channel_read_key +
            '&timezone=' + timezone +
            '&start=' + middle.strftime(format='%Y-%m-%d %H:%M:%S') +
            '&end=' + end.strftime(format='%Y-%m-%d %H:%M:%S'))
        r = requests.get(url)
        # update for loop
        end = middle
        middle = middle - dt.timedelta(minutes=4000)
        # if request goes through then extract feed data
        if 'status' in r.json().keys():
            continue
        curr_df = pd.DataFrame(r.json()['feeds'])
        # if feeds data is not empty then continue processing
        if curr_df.empty:
            continue
        # setting index and cleaning
        curr_df['datetime'] = pd.to_datetime(
            curr_df['created_at']).apply(lambda x: x.astimezone(
                pytz.timezone(timezone)))
        curr_df.set_index('datetime', inplace=True)
        del curr_df['created_at']
        # append current dataframe to df list
        df_list.append(curr_df)
    # if an API request comes back empty we'll get a KeyError, also
    # a request can come back that doesn't throw a key error but is
    # still empty, in these cases raise KeyError so later function
    # knows no data was found for request
    if len(df_list) == 0:
        raise(KeyError)
    df = pd.concat(df_list)
    if df.dropna().empty:
        raise(KeyError)
    # cleaning/processing/labeling
    df.sort_index(inplace=True)
    df = df.apply(pd.to_numeric)
    df = df.groupby(df.index).first()
    del df['entry_id']
    df.dropna(inplace=True)
    df.columns = [0]
    df = df[df.index.to_series().dt.date >= install_date]
    df = df[df.index.to_series().dt.date <= removal_date]
    return df


def get_field_df(thingspeak_tag, input_label, install_date, removal_date):
    """
    helper function to request timeseries data for specific
    device/field/time period using minimum set of inputs
    """
    # processing
    channel_id, channel_read_key = get_channel_data(thingspeak_tag)
    field_id = get_field_id(channel_id, channel_read_key, input_label)
    df = get_field_timeseries_data(
        channel_id, channel_read_key, field_id,
        install_date, removal_date)
    return df


if __name__ == '__main__':
    get_channel_tag_id_dict()
