# -*- coding: utf-8 -*-
"""
some useful functions for pulling thingspeak data from a device
"""
import pandas as pd
import datetime as dt
import requests
import pytz
import json
import os
from dotenv import load_dotenv, find_dotenv

load_dotenv(find_dotenv(usecwd=True))
root = os.environ.get('ROOT')
particle_token = os.environ.get('PT_TK')
ts_api_key = os.environ.get('TS_API_KEY')


def get_channel_duplicates():
    """
    """
    # request channel list from thingspeak
    url = 'https://api.thingspeak.com/channels.json?api_key=' + ts_api_key
    r = requests.get(url)
    channel_list = r.json()
    # iterate over channels and create map of channel ids and "tags" or
    # thingspeak ids
    df = pd.DataFrame(columns=['id', 'tag'])
    for channel in channel_list:
        channel_id = str(channel['id'])
        channel_tag = channel['tags'][0]['name']
        df = df.append(
            pd.DataFrame(
                columns=['id', 'tag'],
                data=[[channel_id, channel_tag]]))
    df['duplicated'] = df['tag'].duplicated(keep=False)
    df.to_csv(
        root + '/data/raw/thingspeak/duplicated_channels.csv', index=False)


def get_device_id(device_name):
    """
    """
    # request list of all devices from particle
    base_url = 'https://api.particle.io/v1'
    end_point = 'devices'
    request_url = base_url + '/' + end_point
    r = requests.get(
        request_url,
        headers={'Authorization': 'Bearer ' + particle_token})
    device_list = r.json()
    # iterate over device list until device name is found
    for curr_device in device_list:
        curr_device_id = curr_device['id']
        curr_device_name = curr_device['name']
        # once device name is found, get device id and return
        if curr_device_name == device_name:
            device_id = curr_device_id
            return device_id
    # if devic name not found, print message and return null
    print('Device name ' + device_name + ' not found on particle.')
    return None


def get_thingspeak_id_list(device_id):
    """
    """
    # get combined readings for device id and parse thingspeak IDs
    base_url = 'https://api.particle.io/v1'
    end_point = 'devices/' + device_id + '/combined readings'
    request_url = base_url + '/' + end_point
    r = requests.get(
        request_url,
        headers={'Authorization': 'Bearer ' + particle_token})
    readings_string = r.json()['result']
    # json from particle gets cutoff causing error, fixed here
    readings_string_corrected = (
        readings_string[:-1] + '}]' + readings_string[-1:])
    # get data readings from json and parse thingspeak IDs as a list
    readings_dict = json.loads(readings_string_corrected)
    readings_data = readings_dict['data']
    thingspeak_id_list = list(set([i['id'].lower() for i in readings_data]))
    return thingspeak_id_list


def get_channel_data(thingspeak_id):
    """
    to request timeseries data, the ThingSpeak API requires a channel ID,
    channel read key, and field number. channel ID and read key are requested
    here
    """
    url = 'https://api.thingspeak.com/channels.json?api_key=' + ts_api_key
    r = requests.get(url)
    channel_list = r.json()
    channel = [
        i for i in channel_list if i['tags'][0]['name'] == thingspeak_id][0]
    channel_id = str(channel['id'])
    channel_api_keys = [x['api_key'] for x in channel['api_keys']]
    channel_read_key = channel_api_keys[1]
    return (channel_id, channel_read_key)


def get_feed_data(channel_id, channel_read_key):
    """
    """
    field_description_fulcrum_label_dict = pd.read_csv(
        root + '/data/raw/thingspeak/field_descriptions.csv',
        dtype=str, index_col='key')['value'].to_dict()
    timezone = 'America/New_York'
    start = (dt.datetime.now(
        tz=pytz.timezone(timezone)) - dt.timedelta(seconds=60)).strftime(
            format='%Y-%m-%d %H:%M:%S')
    end = dt.datetime.now(
        tz=pytz.timezone(timezone)).strftime(format='%Y-%m-%d %H:%M:%S')
    url = (
        'https://api.thingspeak.com/channels/' + channel_id +
        '/feeds.json?api_key=' + channel_read_key +
        '&timezone=' + timezone +
        '&start=' + start +
        '&end=' + end +
        '&metadata=true')
    r = requests.get(url)
    field_id_list = [
        i for i in r.json()['channel'].keys() if 'field' in i]
    field_label_dict = dict(
        filter(lambda x: 'field' in x[0], r.json()['channel'].items()))
    for key, value in field_label_dict.items():
        if 'Temperature' in value:
            field_label_dict[key] = value.split(' ')[0]
    for key, value in field_label_dict.items():
        field_label_dict[key] = field_description_fulcrum_label_dict[value]
    return field_id_list, field_label_dict


def get_field_timeseries_data(thingspeak_id, channel_id, channel_read_key,
                              field_id, field_label,
                              project_start_date, timezone):
    """
    """
    field_number = field_id.split('field')[1]
    # convert project start date to datetime
    start = pd.to_datetime(
        str(project_start_date.year) + '-' +
        str(project_start_date.month) + '-' +
        str(project_start_date.day - 1) +
        'T00:00:00Z').astimezone(
            tz=timezone)
    end = dt.datetime.now(tz=pytz.timezone(timezone))
    middle = end - dt.timedelta(minutes=4000)
    # request timeseries data in 4000 minute intervals (max available from
    # thingspeak in one request)
    field_df_list = list()
    while start < middle:
        url = (
            'https://api.thingspeak.com/' +
            'channels/' + channel_id +
            '/fields/' + field_number +
            '.json?api_key=' + channel_read_key +
            '&timezone=' + timezone +
            '&start=' + middle.strftime(format='%Y-%m-%d %H:%M:%S') +
            '&end=' + end.strftime(format='%Y-%m-%d %H:%M:%S'))
        r = requests.get(url)
        end = middle
        middle = middle - dt.timedelta(minutes=4000)
        # if request goes through then extract feed data
        if 'status' in r.json().keys():
            continue
        field_df = pd.DataFrame(r.json()['feeds'])
        # if feeds data is not empty then continue processing
        if field_df.empty:
            continue
        # setting index and cleaning
        field_df['datetime'] = pd.to_datetime(
            field_df['created_at']).apply(lambda x: x.astimezone(
                pytz.timezone(timezone)))
        field_df.set_index('datetime', inplace=True)
        del field_df['created_at']
        # append current dataframe to df list
        field_df_list.append(field_df)
    if len(field_df_list) == 0:
        print('No field data returned from ThingSpeak')
        return pd.DataFrame()
    field_df = pd.concat(field_df_list)
    if field_df.empty:
        print('No field data returned from ThingSpeak')
        return pd.DataFrame()
    # cleaning/processing
    field_df.sort_index(inplace=True)
    field_df = field_df.apply(pd.to_numeric)
    field_df = field_df.groupby(field_df.index).first()
    del field_df['entry_id']
    field_df.dropna(inplace=True)
    field_df.columns = [thingspeak_id + '_' + field_label]
    return field_df


def get_device_readings(device_name, project_start_date, timezone):
    """
    """
    device_df = pd.DataFrame()
    device_id = get_device_id(device_name)
    thingspeak_id_list = get_thingspeak_id_list(device_id)
    for thingspeak_id in thingspeak_id_list:
        (channel_id, channel_read_key) = get_channel_data(thingspeak_id)
        field_id_list, field_label_dict = get_feed_data(
            channel_id, channel_read_key)
        for field_id in field_id_list:
            field_label = field_label_dict[field_id]
            field_df = get_field_timeseries_data(
                thingspeak_id,
                channel_id, channel_read_key,
                field_id, field_label,
                project_start_date, timezone)
            if field_df.empty:
                continue
            device_df = pd.merge(
                device_df, field_df,
                left_index=True, right_index=True, how='outer')
    return device_df


def get_device_readings_outer():
    """
    """
    device_name = 'G157'
    project_start_date = dt.date(2022, 1, 25)
    timezone = 'America/New_York'
    device_df = get_device_readings(device_name, project_start_date, timezone)
    device_df.to_csv('c:/users/ari.jackson/desktop/' + device_name + '.csv')
