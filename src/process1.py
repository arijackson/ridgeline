# -*- coding: utf-8 -*-
"""
processing fulcrum data

merging data needed for analysis into single flat file, geocoding addressed
and merging enrollment numbers with fulcrum data
"""
import pandas as pd
import numpy as np
import datetime as dt
import warnings
import os
from dotenv import load_dotenv, find_dotenv

load_dotenv(find_dotenv(usecwd=True))
root = os.environ.get('ROOT')
pd.options.mode.chained_assignment = None  # default='warn'
warnings.simplefilter(action='ignore', category=pd.errors.PerformanceWarning)


def sentient_things_devices_to_processed():
    """
    compiling all attributes collected in fulcrum needed to download, process,
    and merge thingspeak data for each device and measurement
    """
    # Create df with index id0 through id4 that appear in fulcrum files,
    # each row to represent attributes of an indoor unit and its
    # supplying outdoor unit
    df3 = pd.read_hdf(
        root + '/data/interim/fulcrum/' +
        'ridgeline_sentientthings_devices_measurements.h5')
    df2 = pd.read_hdf(
        root + '/data/interim/fulcrum/ridgeline_sentientthings_devices.h5')
    df0 = pd.read_hdf(
        root + '/data/interim/fulcrum/ridgeline.h5')
    # use id0, id2, and id3 fom df1
    df = df3[['id0', 'id2', 'id3']]
    # set indicies from fulcrum files to map attributes to df
    df3.set_index('id3', inplace=True)
    df2.set_index('id2', inplace=True)
    df0.set_index('id0', inplace=True)
    # map fields needed for analysis from each of the five fulcrum files
    df['site_label'] = df['id0'].map(
        df0['site_label'].to_dict())
    df['thingspeak_tag'] = df['id2'].map(
        df2['thingspeak_tag'].to_dict())
    df['particle_device_name'] = df['id2'].map(
        df2['particle_device_name'].to_dict())
    df['install_date'] = df['id2'].map(
        df2['install_date_st_device'].to_dict())
    df['removal_date'] = df['id2'].map(
        df2['removal_date_st_device'].to_dict())
    df['equipment_label'] = df['id3'].map(
        df3['equipment_label_st_device'].to_dict())
    df['measurement_label'] = df['id3'].map(
        df3['measurement_label_st_device'].to_dict())
    df['measurement_quantity'] = df['id3'].map(
        df3['measurement_quantity_st_device'].to_dict())
    df['input_label'] = df['id3'].map(
        df3['input_label_st_device'].to_dict())
    del df['id0'], df['id2'], df['id3']
    df['install_date'] = pd.to_datetime(df['install_date']).dt.date
    df['removal_date'] = pd.to_datetime(df['removal_date']).dt.date
    df['removal_date'] = np.where(
        pd.isnull(df['removal_date']),
        dt.datetime.now().date(),
        df['removal_date'])
    # update LoRa thingspeak tags to include gateway thingspeak tags
    index_to_del_list = list()
    for index, row in df.iterrows():
        particle_device_name = row['particle_device_name']
        if particle_device_name != 'LORA':
            continue
        site_label = row['site_label']
        thingspeak_tag = row['thingspeak_tag']
        install_date = row['install_date']
        removal_date = row['removal_date']
        equipment_label = row['equipment_label']
        measurement_label = row['measurement_label']
        measurement_quantity = row['measurement_quantity']
        input_label = row['input_label']
        # for each row, figure out what gateway was inplace at that time
        # update outdoor id
        curr_df = df[df['site_label'] == site_label]
        gateway_thingspeak_tag_list = curr_df[
            curr_df['particle_device_name'].str.contains('G')][
            'thingspeak_tag'].unique().tolist()
        for gateway_thingspeak_tag in gateway_thingspeak_tag_list:
            gateway_install_date = curr_df[
                curr_df['thingspeak_tag'] == gateway_thingspeak_tag][
                    'install_date'].iloc[0]
            gateway_removal_date = curr_df[
                curr_df['thingspeak_tag'] == gateway_thingspeak_tag][
                    'removal_date'].iloc[0]
            # check if lora and that lora and gateway overlapped
            if ((removal_date > gateway_install_date) or
               (install_date < gateway_removal_date)):
                install_date_max = max(install_date, gateway_install_date)
                removal_date_min = min(removal_date, gateway_removal_date)
                vals = [
                    site_label,
                    thingspeak_tag + '-' + gateway_thingspeak_tag,
                    particle_device_name,
                    install_date_max, removal_date_min, equipment_label,
                    measurement_label, measurement_quantity, input_label]
                df = df.append(pd.DataFrame(columns=df.columns, data=[vals]))
                index_to_del_list.append(index)
    index_to_del_list = list(set(index_to_del_list))
    for index in index_to_del_list:
        df = df.drop(index)
    df.dropna(
        subset=[
            'thingspeak_tag', 'install_date', 'removal_date', 'input_label'],
        how='any', inplace=True)
    # write to csv/h5
    df.to_hdf(
        root + '/data/processed/fulcrum/device_data.h5', 'df')
    df.to_csv(
        root + '/data/processed/fulcrum/device_data.csv', index=False)


def ct_to_processed():
    """
    """
    df1 = pd.read_hdf(
        root + '/data/interim/fulcrum/ridgeline_cts.h5')
    df0 = pd.read_hdf(
        root + '/data/interim/fulcrum/ridgeline.h5')
    # use id0, id2, and id3 fom df1
    df = df1[['id0', 'id2']]
    # set indicies from fulcrum files to map attributes to df
    df1.set_index('id2', inplace=True)
    df0.set_index('id0', inplace=True)
    # map fields needed for analysis from each of the five fulcrum files
    df['site_label'] = df['id0'].map(df0['site_label'].to_dict())
    df['equipment_label'] = df['id2'].map(df1['equipment_label_ct'].to_dict())
    df['measurement_label'] = df['id2'].map(
        df1['measurement_label_ct'].to_dict())
    df['measurement_quantity'] = df['id2'].map(
            df1['measurement_quantity_ct'].to_dict())
    df['amperage'] = df['id2'].map(df1['amperage_ct'].to_dict())
    df['install_date'] = df['id2'].map(df1['install_date_ct'].to_dict())
    df['removal_date'] = df['id2'].map(df1['removal_date_ct'].to_dict())
    del df['id0'], df['id2']
    df['install_date'] = pd.to_datetime(df['install_date']).dt.date
    df['removal_date'] = pd.to_datetime(df['removal_date']).dt.date
    df['removal_date'] = np.where(
        pd.isnull(df['removal_date']),
        dt.datetime.now().date(),
        df['removal_date'])
    df.to_hdf(
        root + '/data/processed/fulcrum/ct_data.h5', 'df')
    df.to_csv(
        root + '/data/processed/fulcrum/ct_data.csv', index=False)


def wattnode_to_processed():
    """
    """
    df1 = pd.read_hdf(
        root + '/data/interim/fulcrum/ridgeline_wattnodes.h5')
    df0 = pd.read_hdf(
        root + '/data/interim/fulcrum/ridgeline.h5')
    # use id0, id2, and id3 fom df1
    df = df1[['id0', 'id2']]
    # set indicies from fulcrum files to map attributes to df
    df1.set_index('id2', inplace=True)
    df0.set_index('id0', inplace=True)
    # map fields needed for analysis from each of the five fulcrum files
    df['site_label'] = df['id0'].map(df0['site_label'].to_dict())
    df['equipment_label'] = df['id2'].map(
        df1['equipment_label_wattnode'].to_dict())
    df['measurement_label'] = df['id2'].map(
        df1['measurement_label_wattnode'].to_dict())
    df['measurement_quantity'] = df['id2'].map(
            df1['measurement_quantity_wattnode'].to_dict())
    df['voltage'] = df['id2'].map(df1['voltage_wattnode'].to_dict())
    df['install_date'] = df['id2'].map(df1['install_date_wattnode'].to_dict())
    df['removal_date'] = df['id2'].map(df1['removal_date_wattnode'].to_dict())
    del df['id0'], df['id2']
    df['install_date'] = pd.to_datetime(df['install_date']).dt.date
    df['removal_date'] = pd.to_datetime(df['removal_date']).dt.date
    df['removal_date'] = np.where(
        pd.isnull(df['removal_date']),
        dt.datetime.now().date(),
        df['removal_date'])
    df.to_hdf(
        root + '/data/processed/fulcrum/wattnode_data.h5', 'df')
    df.to_csv(
        root + '/data/processed/fulcrum/wattnode_data.csv', index=False)


def equipment_to_processed():
    """
    """
    df2 = pd.read_hdf(
        root + '/data/interim/fulcrum/ridgeline_equipment.h5')
    df0 = pd.read_hdf(
        root + '/data/interim/fulcrum/ridgeline.h5')
    df = df2.loc[:, ~df2.columns.isin(['id1', 'id2'])]
    df0.set_index('id0', inplace=True)
    df['site_label'] = df['id0'].map(
        df0['site_label'].to_dict())
    # write to csv/h5
    df.to_hdf(
        root + '/data/processed/fulcrum/equipment_data.h5', 'df')
    df.to_csv(
        root + '/data/processed/fulcrum/equipment_data.csv', index=False)


if __name__ == '__main__':
    sentient_things_devices_to_processed()
    ct_to_processed()
    wattnode_to_processed()
    equipment_to_processed()
