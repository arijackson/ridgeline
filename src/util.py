# -*- coding: utf-8 -*-
"""
"""
import pandas as pd
import process2
import time
from geopy import geocoders
import qc_plot
import importlib
import os
from dotenv import load_dotenv, find_dotenv

load_dotenv(find_dotenv(usecwd=True))
root = os.environ.get('ROOT')
bing_api_key = os.environ.get('BINGAPIKEY')
importlib.reload(process2)
importlib.reload(qc_plot)


def run():
    # index sampled at two minute intervals in Eastern timezone
    freq = '2T'
    tz = 'America/New_York'
    site_label = input(
        "Enter the site label you'd like to process. "
        "To process all sites, type all: ")
    if site_label == 'all':
        df_fulcrum = pd.read_hdf(
            root + '/data/processed/fulcrum/device_data.h5')
        site_list = df_fulcrum['site_label'].unique().tolist()
        i_0 = 0
        i_n = len(site_list)
        i = i_0
        print('')
        print('Processing Data')
        for site_label in site_list[i_0:i_n]:
            i += 1
            print('')
            print(str(i) + ' of ' + str(i_n) + ', ' + site_label)
            try:
                process2.outer(site_label, freq, tz)
            except:				# noqa
                j = 0
                while j < 3:
                    time.sleep(60)
                    j += 1
                    try:
                        process2.outer(site_label, freq, tz)
                    except:		# noqa
                        continue
                continue
        i_0 = 0
        i_n = len(site_list)
        i = i_0
        print('')
        print('Plotting Data')
        for site_label in site_list[i_0:i_n]:
            i += 1
            print('')
            print(str(i) + ' of ' + str(i_n) + ', ' + site_label)
            try:
                qc_plot.site_qc(site_label, show=False)
            except:				# noqa
                continue
        qc_plot.merge_site_pdfs()
    else:
        process2.outer(site_label, freq, tz)
        show = input(
            "Would you like to see the plot? (yes/no): ")
        if show.lower() == 'yes':
            qc_plot.site_qc(site_label, show=True)
        else:
            qc_plot.site_qc(site_label, show=False)


def geocode_addresses():
    """
    Get Lat/Lon of scheduled sites in study, coords used for mapping
    """
    # instantiate dataframe to populate
    df = pd.read_csv(root + '/data/interim/fulcrum/ridgeline.csv', dtype=str)
    df['address'] = (
        df['address'] + ', ' + df['town'] + ', ' + df['state'])
    df = df[['site_label', 'address']]
    df['county'] = 'na'
    df['lat'] = 'na'
    df['lon'] = 'na'
    g = geocoders.Bing(api_key=bing_api_key)
    print('')
    print('    Geocoding addresses.')
    for index, row in df.iterrows():
        address = row['address']
        time.sleep(1)
        try:
            loc = g.geocode(address)
            lat = loc.latitude
            lon = loc.longitude
            county = loc.raw['address']['adminDistrict2'].split(' ')[0]
            df.at[index, 'county'] = county
            df.at[index, 'lat'] = lat
            df.at[index, 'lon'] = lon
        except:  # noqa, GeocoderQuotaExceeded
            print("        Couldn't geocode address for site label " +
                  str(index) + '.')
            continue
    print('')
    df.to_csv(root + '/data/raw/project/site_coords.csv', index=False)
    df.to_csv(root + '/data/interim/project/site_coords.csv', index=False)


if __name__ == '__main__':
    run()
