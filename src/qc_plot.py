# -*- coding: utf-8 -*-
"""
generating plots for data qc
"""
import pandas as pd
import datetime as dt
import matplotlib
import matplotlib.dates as mdates
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
from PyPDF2 import PdfFileMerger
import os
from dotenv import load_dotenv, find_dotenv

load_dotenv(find_dotenv(usecwd=True))
root = os.environ.get('ROOT')


def site_qc(site_label, show):
    """
    plot time series data for all indoor and outdoor devices used at a site
    """
    fig_list = list()
    # load time series data
    df = pd.read_hdf(
        root + '/data/processed/thingspeak/sites/' + site_label + '.h5')
    # load device data and get install and removal dates
    df_device = pd.read_hdf(root + '/data/processed/fulcrum/device_data.h5')
    df_device = df_device[df_device['site_label'] == site_label]
    install_date = df_device['install_date'].min()
    download_date = (df_device['removal_date'].max() + dt.timedelta(days=1))
    # load fulcrum equipment data to add to plot title
    df_equipment = pd.read_hdf(
        root + '/data/processed/fulcrum/equipment_data.h5')
    df_equipment = df_equipment[df_equipment['site_label'] == site_label]
    equipment_description_dict = df_equipment.set_index(
        'equipment_label')['equipment_description'].to_dict()
    area_served_dict = df_equipment.set_index(
        'equipment_label')['area_served'].to_dict()
    # create one plot for each time series
    for column in df.columns:
        s = df[column]
        equipment_label = ' '.join([
            i.capitalize()
            if not i[0].isdigit()
            else i
            for i in column.split(' - ')[0].split(' ')])
        plt.close('all')
        fig, ax = plt.subplots(nrows=1, sharex=True, figsize=(12, 6))
        # plot download/install dates for each axes, add title and xlabel
        ax.axvline(
            install_date,
            color='k', lw=1, ls='--', label=None)
        ax.axvline(
            download_date,
            color='k', lw=1, ls='--', label=None)
        try:
            equipment_description = equipment_description_dict[equipment_label]
        except KeyError:
            equipment_description = ''
        try:
            area_served = area_served_dict[equipment_label]
        except KeyError:
            area_served = ''
        ax.set_title(
            'Site ID: ' + site_label + '\n' +
            'Equipment Label: ' +
            equipment_label.upper() +
            ', ' +
            'Equipment Description: ' +
            ' '.join([i.capitalize() for i
                      in equipment_description.split(' ')]) +
            ', ' +
            'Area Served: ' +
            ' '.join([i.capitalize() for i in str(area_served).split(' ')]),
            fontsize=10)
        fmt_week = mdates.DayLocator(interval=7)
        ax.xaxis.set_major_locator(fmt_week)
        fmt_day = mdates.DayLocator()
        ax.xaxis.set_minor_locator(fmt_day)
        ax.xaxis.set_major_formatter(mdates.DateFormatter('%m/%d'))

        unit_dict = {
            'Power': '[kW]',
            'Current': '[A]',
            'Temperature': '[F]',
            'Relative Humidity': '[%]',
            'Voltage': '[V]'}

        # helper function for formatting
        def plot_measurement(ax, df, legend_label, y_label, color):
            ax.plot(
                df.index.to_series().apply(lambda x: x.replace(tzinfo=None)),
                df.values, label=legend_label,
                color=color, lw=1, rasterized=True)
            ax.set_ylabel(y_label + ' ' + unit_dict[y_label], fontsize=6)
            ax.tick_params(labelsize=6)
            # after about 3/4s of a year the x-axis font needs to be shrunk a
            # little to avoid overlapping
            if len(s) > 60 / 2 * 24 * 7 * 52 * .75:
                ax.tick_params(axis='x', labelsize=5)
            ax.set_xlabel(None)
            leg = ax.legend(loc='upper left', fontsize=6, framealpha=1)
            frame = leg.get_frame()
            frame.set_facecolor('white')
            frame.set_edgecolor('black')

        # plot each measurement
        plot_measurement(
            ax, s,
            equipment_label + ' ' +
            ' '.join(
                [i.capitalize()
                 for i in
                 column.split(' - ')[1].split(' ')]),
            ' '.join(
                [i.capitalize()
                 for i in
                 column.split(' - ')[-1].split(' ')]),
            'k')
        # maximize/tighten frame, display if show is true otherwise save pdf
        plt.tight_layout()
        fig_list.extend(
            [manager.canvas.figure
             for manager in
             matplotlib._pylab_helpers.Gcf.get_all_fig_managers()])
        if show is True:
            plt.show()
        else:
            plt.close('all')
    # combine plots of indoor units to write pdf for each site
    pdf_path = (
        root + '/data/processed/plots/sites/' +
        site_label + '.pdf')
    pdf = PdfPages(pdf_path)
    for fig in fig_list:
        pdf.savefig(fig)
    pdf.close()


def merge_site_pdfs():
    """
    merge all site pdfs into one
    """
    dir_path = root + '/data/processed/plots/sites'
    pdfs = os.listdir(dir_path)
    df = pd.DataFrame()
    df['file_name'] = pdfs
    df['site_label'] = df['file_name'].str.split('.').str[0].apply(int)
    df.sort_values(by='site_label', ascending=True, inplace=True)
    pdfs = df['file_name'].values.tolist()
    merger = PdfFileMerger()
    for pdf in pdfs:
        merger.append(dir_path + '/' + pdf)
    merger.write(root + '/data/processed/plots/site_qc.pdf')
    merger.close()


if __name__ == '__main__':
    merge_site_pdfs()
