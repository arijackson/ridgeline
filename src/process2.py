# -*- coding: utf-8 -*-
"""
processing thingspeak data
"""
import pandas as pd
import numpy as np
import warnings
import datetime as dt
from preprocess2 import get_field_df
import os
from dotenv import load_dotenv, find_dotenv

load_dotenv(find_dotenv(usecwd=True))
root = os.environ.get('ROOT')
ts_api_key = os.environ.get('TS_API_KEY')
pd.options.mode.chained_assignment = None
warnings.filterwarnings('ignore', category=pd.io.pytables.PerformanceWarning)


def check_cumulative(df):
    """
    some thingspeak devices report pulse counts cumulatively, for these
    devices the data needs to be differenced to calculate the pulses
    recorded in a single time interval, this function checks if the
    recorded pulses are non-decreasing, and if they are calculates
    the differences
    """
    if all(
        x <= y for x, y in
            zip(df.iloc[:, 0].values, df.iloc[:, 0].values[1:])):
        df.iloc[:, 0] = df.iloc[:, 0].diff()
    return df


def check_pulse_quality(df):
    """
    basic threshold checks for pulse counts
    """
    # filter negative values used as place holder for missing data
    df.iloc[:, 0] = np.where(
        df.iloc[:, 0] < 0,
        np.nan,
        df.iloc[:, 0])
    # filter readings greater than 3 standard deviations from mean
    df.iloc[:, 0] = np.where(
        df.iloc[:, 0] > df.iloc[:, 0].mean() + 3 * df.iloc[:, 0].std(),
        np.nan,
        df.iloc[:, 0])
    return df


def calc_energy(option, model, ct_amps):
    """
    see wattnode docs, use function to convert puses to energy
    """
    # phases per pulse output
    pppo_dict = {
        'Standard': 3,
        'Option DPO': 3,
        'Option P3': 1,
        'Option PV': 2}			# PV is 2 for P1 and P2, 1 for P3
    pppo = pppo_dict[option]
    # nominal line voltage
    nvac_dict = {
        'WNB-3Y-208-P': 120.,
        'WNB-3Y-400-P': 230.,
        'WNB-3Y-480-P': 277.,
        'WNB-3Y-600-P': 347.,
        'WNB-3D-240-P': 120.,
        'WNB-3D-400-P': 230.,
        'WNB-3D-480-P': 277.}
    nvac = model.map(nvac_dict)
    # full-scale pulse frequency
    fshz_dict = {
        'WNB-3Y-208-P': 4.,
        'WNB-3Y-400-P': 4.,
        'WNB-3Y-480-P': 4.,
        'WNB-3Y-600-P': 4.,
        'WNB-3D-240-P': 4.,
        'WNB-3D-400-P': 4.,
        'WNB-3D-480-P': 4.}
    fshz = model.map(fshz_dict)
    # kilowatt hours per pulse
    kwhpp = (pppo * nvac * ct_amps) / (fshz * 3600. * 1000.)
    return kwhpp


def resample_power(df, ct_amps, wattnode_voltage, freq):
    """
    convert pulses to kW using CT amps, wattnode voltage, and
    frequency in minutes, resample to specisified frequency
    """
    df = check_cumulative(df)
    df = df.resample(freq).apply(lambda x: x.sum(skipna=True))
    option = 'Standard'
    model = (
        'WNB-3D-' +
        wattnode_voltage.apply(int).apply(str) +
        '-P')
    kwhpp = calc_energy(option, model, ct_amps)
    # [pulse/1][kwh/pulse][min/h][min/1]
    df = (
        df.iloc[:, 0] *
        kwhpp *
        60.0 /
        float(freq.split('T')[0])).to_frame()
    df = check_pulse_quality(df)
    return df


def resample_current(df, ct_amps, freq):
    """
    convert pulses to current using CT amps and frequency in minutes,
    resample to specisified frequency
    """
    df = check_cumulative(df)
    df = df.resample(freq).apply(lambda x: x.sum(skipna=True))
    # ct to pulse boxes report low values due to input impedance,
    # for 1 A CTs values are 69% of true value and for 5 A CTs
    # values are 91% of true value
    scale_correction_map = {1.0: .69, 5.0: .91}
    scale_correction = ct_amps.map(scale_correction_map)
    # 1 pulse = 1 mV, 1 A = 333 mV
    df = (
        df.iloc[:, 0] /
        333. *
        ct_amps /
        float(freq.split('T')[0]) /
        scale_correction).to_frame()
    df = check_pulse_quality(df)
    return df


def resample_non_pulse(df, freq):
    """
    resample to specisified frequency, use for measurements other than
    pulse counts (temps, voltages, rh, etc)
    """
    df = df.resample(freq).apply(lambda x: x.mean(skipna=False))
    df.fillna(inplace=True, method='ffill', limit=1)
    df.fillna(inplace=True, method='bfill', limit=1)
    return df


def ct_amps_to_processed(site_label, freq, tz):
    """
    create time series of CT sizes for each measurement taken with a CT
    """
    df_ct = pd.read_hdf(root + '/data/processed/fulcrum/ct_data.h5')
    df_ct = df_ct[df_ct['site_label'] == site_label]
    df_ct['merge_var'] = (
        df_ct['site_label'] + ' - ' +
        df_ct['equipment_label'] + ' - ' +
        df_ct['measurement_label'] + ' - ' +
        df_ct['measurement_quantity'])
    merge_var_list = df_ct['merge_var']
    for merge_var in merge_var_list:
        curr_df = df_ct[df_ct['merge_var'] == merge_var]
        df = pd.DataFrame(columns=['ct_amps'])
        for index, row in curr_df.iterrows():
            ct_amps = float(row['amperage'].split(' ')[0])
            install_date = row['install_date']
            removal_date = row['removal_date']
            df = df.append(
                pd.DataFrame(
                    index=pd.date_range(
                        install_date,
                        removal_date + dt.timedelta(days=1),
                        freq=freq, tz=tz),
                    data=[[ct_amps]], columns=['ct_amps']))
        file_dst = (
            root + '/data/processed/thingspeak/ct_amps/' + merge_var + '.h5')
        df['ct_amps'].to_hdf(file_dst, 'df')


def wattnode_voltage_to_processed(site_label, freq, tz):
    """
    create time series of Wattnode sizes for each measurement taken with a WN
    """
    df_wn = pd.read_hdf(root + '/data/processed/fulcrum/wattnode_data.h5')
    df_wn = df_wn[df_wn['site_label'] == site_label]
    df_wn['merge_var'] = (
        df_wn['site_label'] + ' - ' +
        df_wn['equipment_label'] + ' - ' +
        df_wn['measurement_label'] + ' - ' +
        df_wn['measurement_quantity'])
    merge_var_list = df_wn['merge_var']
    for merge_var in merge_var_list:
        curr_df = df_wn[df_wn['merge_var'] == merge_var]
        df = pd.DataFrame(columns=['wattnode_voltage'])
        for index, row in curr_df.iterrows():
            wattnode_voltage = float(row['voltage'].split(' ')[0])
            install_date = row['install_date']
            removal_date = row['removal_date']
            df = df.append(
                pd.DataFrame(
                    index=pd.date_range(
                        install_date,
                        removal_date + dt.timedelta(days=1),
                        freq=freq, tz=tz),
                    data=[[wattnode_voltage]], columns=['wattnode_voltage']))
        file_dst = (
            root + '/data/processed/thingspeak/wattnode_voltage/' +
            merge_var + '.h5')
        df['wattnode_voltage'].to_hdf(file_dst, 'df')


def site_to_interim(site_label, freq, tz):
    """
    iterate over each measurement take on site and store time series data for
    each quantity
    """
    # filter fulcrum data for site id
    df_device = pd.read_hdf(root + '/data/processed/fulcrum/device_data.h5')
    df_device = df_device[df_device['site_label'] == site_label]
    # iterate over list of outdoor unit labels
    df_device['merge_var'] = (
        df_device['site_label'] + ' - ' +
        df_device['equipment_label'] + ' - ' +
        df_device['measurement_label'] + ' - ' +
        df_device['measurement_quantity'])
    merge_var_list = df_device['merge_var'].unique().tolist()
    for merge_var in merge_var_list:
        curr_df = df_device[df_device['merge_var'] == merge_var]
        file_dst = root + '/data/interim/thingspeak/' + merge_var + '.h5'
        # check if dataframe already exists
        if os.path.exists(file_dst):
            # if data has already been downloaded then load the existing df
            df_ext = pd.read_hdf(file_dst)
            if not df_ext.empty:
                # set start date for current download to end data of
                # past download
                install_date = df_ext.index[-1].date()
        thingspeak_df_list = list()
        for index, row in curr_df.iterrows():
            thingspeak_tag = row['thingspeak_tag']
            input_label = row['input_label']
            removal_date = row['removal_date']
            if (not os.path.exists(file_dst)) or (df_ext.empty):
                install_date = row['install_date']
            # download data for specific device/measurement/period of time, if
            # thingspeak doesn't return any data ("KeyError") then create dummy
            # dataframe to keep track of nulls
            try:
                thingspeak_df = get_field_df(
                    thingspeak_tag, input_label, install_date, removal_date)
            except KeyError:
                # because pulses use nans to imply zero, fill dummy df with -1
                if ('P' in input_label):
                    fill_val = -1
                # otherwise use nans for temps/rh/voltages/etc.
                else:
                    fill_val = np.nan
                thingspeak_df = pd.DataFrame(
                    fill_val,
                    index=pd.date_range(
                        install_date, removal_date, tz=tz, freq=freq),
                    columns=[0])
            # append data for particular device/point in time to list for
            # concatenation
            thingspeak_df_list.append(thingspeak_df)
        # concat dfs from all devices as continuous measurement of
        # specisified quantity
        df = pd.concat(thingspeak_df_list)
        # combine already downloaded data if present
        if os.path.exists(file_dst):
            df = pd.concat([df_ext, df])
            df.sort_index(inplace=True)
            df = df[~df.index.duplicated(keep='first')]
        # write data to interim
        df.to_hdf(file_dst, 'df')


def site_to_processed(site_label, freq):
    """
    combine all measurements take on site into single dataframe,
    resample and post-postprocess thinspeak data
    """
    # merge all available data for site
    site_df = pd.DataFrame()
    dir_path = root + '/data/interim/thingspeak'
    file_list = os.listdir(dir_path)
    file_list = [i for i in file_list if i.split(' - ')[0] == site_label]
    for file_name in file_list:
        df = pd.read_hdf(dir_path + '/' + file_name)
        quantity = file_name.split(' - ')[3].split('.')[0]
        if quantity == 'POWER':
            ct_amps = pd.read_hdf(
                root + '/data/processed/thingspeak/ct_amps/' + file_name)
            wattnode_voltage = pd.read_hdf(
                root + '/data/processed/thingspeak/wattnode_voltage/' +
                file_name)
            df = resample_power(df, ct_amps, wattnode_voltage, freq)
        elif quantity == 'CURRENT':
            ct_amps = pd.read_hdf(
                root + '/data/processed/thingspeak/ct_amps/' + file_name)
            df = resample_current(df, ct_amps, freq)
        else:
            df = resample_non_pulse(df, freq)
        df.columns = [' - '.join(file_name.split('.')[0].split(' - ')[1:])]
        site_df = pd.merge(
            site_df, df, how='outer', left_index=True, right_index=True)
    site_df.to_hdf(
        root + '/data/processed/thingspeak/sites/' + site_label + '.h5', 'df')
    site_df.to_csv(
        root + '/data/processed/thingspeak/sites/' + site_label + '.csv')


def outer(site_label, freq, tz):
    """
    consolidate functions in process2
    """
    ct_amps_to_processed(site_label, freq, tz)
    wattnode_voltage_to_processed(site_label, freq, tz)
    site_to_interim(site_label, freq, tz)
    site_to_processed(site_label, freq)
