# -*- coding: utf-8 -*-
"""
fulcrum data raw -> interim

unzipping export files and minor processing/parsing/relabeling
"""
import pandas as pd
import datetime as dt
import platform
import warnings
import os
import zipfile
import shutil
from dotenv import load_dotenv, find_dotenv

load_dotenv(find_dotenv(usecwd=True))
root = os.environ.get('ROOT')
pd.options.mode.chained_assignment = None  # default='warn'
warnings.simplefilter(action='ignore', category=pd.errors.PerformanceWarning)


def unzip_fulcrum_export():
    """
    unzip fulcrum export from downloads and move to project directory
    """
    # determine if running on Mac or Windows, then get path to downloads
    system = platform.system()
    if system == 'Windows':
        path_to_downloads = os.path.join(
            os.getenv('USERPROFILE'), 'Downloads').replace('\\', '/').lower()
    elif system == 'Darwin':		# Mac is Darwin
        path_to_downloads = (
            '/Users/' + os.environ.get('USER') + '/Downloads').lower()
    # list Fulcrum exports
    file_list = os.listdir(path_to_downloads)
    fulcrum_file_list = [i for i in file_list if 'Fulcrum_Export' in i]
    # print reminder if no new fulcrum data has been downloaded
    if len(fulcrum_file_list) == 0:
        print('')
        print('    No new Fulcrum export has been downloaded.')
        print('')
    # otherwise extract files from zip folder
    else:
        # get download date for each fulcrum export
        file_date_list = list()
        fulcrum_emt_file_list = list()
        for file_name in fulcrum_file_list:
            zip_file = zipfile.ZipFile(path_to_downloads + '/' + file_name)
            # check if fulcrum export is for app "Ridgeline"
            if 'ridgeline/' not in zip_file.namelist():
                continue
            file_date = dt.datetime.fromtimestamp(
                float(os.path.getatime(
                    path_to_downloads + '/' + file_name)))
            file_date_list.append((file_name, file_date))
            fulcrum_emt_file_list.append(file_name)
        if len(file_date_list) == 0:
            print('')
            print('    No new Fulcrum export has been downloaded.')
            return
        # sort files by download date
        file_date_list_sorted = sorted(
            file_date_list, key=lambda tup: tup[1], reverse=True)
        # get most recenlty downloaded Fulcrum export
        zip_file = file_date_list_sorted[0][0]
        # extract and move unzipped folder to data/raw/fulcrum
        path_to_zip_file = path_to_downloads + '/' + zip_file
        directory_to_extract_to = root + '/data/raw/fulcrum'
        with zipfile.ZipFile(path_to_zip_file, 'r') as zip_ref:
            zip_ref.extractall(directory_to_extract_to)
        # move files from 'emt' folder to data/raw/fulcrum
        for file_name in os.listdir(root + '/data/raw/fulcrum/ridgeline'):
            if '.jpg' not in file_name:
                shutil.move(
                    root + '/data/raw/fulcrum/ridgeline/' + file_name,
                    root + '/data/raw/fulcrum/' + file_name)
            else:
                shutil.move(
                    root + '/data/raw/fulcrum/ridgeline/' + file_name,
                    root + '/data/raw/fulcrum/images/' + file_name)
        # delete fulcrum exports in downloads once most recent download
        # has been extracted
        for file_name in fulcrum_emt_file_list:
            os.remove(path_to_downloads + '/' + file_name)


def preprocess_ridgeline():
    """
    site attribute data processing
    """
    # load export file
    df = pd.read_csv(
        root + '/data/raw/fulcrum/ridgeline.csv', dtype=str)
    # identify columns to keep for analysis
    to_keep = [
        'fulcrum_id',
        'site_label', 'state', 'town', 'address']
    df = df[to_keep]
    # rename identifiers for later merging
    df.rename(columns={'fulcrum_id': 'id0'}, inplace=True)
    df = df.apply(lambda x: x.str.upper() if x.dtype == 'object' else x)
    # write data to interim
    df.to_hdf(root + '/data/interim/fulcrum/ridgeline.h5', 'df')
    df.to_csv(root + '/data/interim/fulcrum/ridgeline.csv', index=False)


def preprocess_ridgeline_sentientthings_devices():
    """
    sentient things device attribute data processing
    """
    # load data
    df = pd.read_csv(
        root + '/data/raw/fulcrum/ridgeline_sentientthings_devices.csv',
        dtype=str)
    to_keep = [
        'fulcrum_id', 'fulcrum_parent_id', 'fulcrum_record_id',
        'thingspeak_tag', 'particle_device_name',
        'install_date_st_device', 'removal_date_st_device']
    df = df[to_keep]
    # relabel identifiers for merging datasets
    df.rename(
        columns={
            'fulcrum_parent_id': 'id1',
            'fulcrum_record_id': 'id0',
            'fulcrum_id': 'id2'},
        inplace=True)
    df = df.apply(lambda x: x.str.upper() if x.dtype == 'object' else x)
    df.to_hdf(
        root + '/data/interim/fulcrum/ridgeline_sentientthings_devices.h5',
        'df')
    df.to_csv(
        root + '/data/interim/fulcrum/ridgeline_sentientthings_devices.csv',
        index=False)


def preprocess_ridgeline_sentientthings_devices_measurements():
    """
    sentient things device attribute data processing
    """
    # load data
    df = pd.read_csv(
        root +
        '/data/raw/fulcrum/ridgeline_sentientthings_devices_measurements.csv',
        dtype=str)
    to_keep = [
        'fulcrum_id', 'fulcrum_parent_id', 'fulcrum_record_id',
        'equipment_label_st_device', 'measurement_label_st_device',
        'measurement_quantity_st_device', 'input_label_st_device']
    df = df[to_keep]
    # relabel identifiers for merging datasets
    df.rename(
        columns={
            'fulcrum_parent_id': 'id2',
            'fulcrum_record_id': 'id0',
            'fulcrum_id': 'id3'},
        inplace=True)
    df = df.apply(lambda x: x.str.upper() if x.dtype == 'object' else x)
    df.to_hdf(
        root + '/data/interim/fulcrum/' +
        'ridgeline_sentientthings_devices_measurements.h5',
        'df')
    df.to_csv(
        root + '/data/interim/fulcrum/' +
        'ridgeline_sentientthings_devices_measurements.csv',
        index=False)


def preprocess_ridgeline_wattnodes():
    """
    sentient things device attribute data processing
    """
    # load data
    df = pd.read_csv(
        root + '/data/raw/fulcrum/ridgeline_wattnodes.csv',
        dtype=str)
    to_keep = [
        'fulcrum_id', 'fulcrum_parent_id', 'fulcrum_record_id',
        'equipment_label_wattnode', 'measurement_label_wattnode',
        'measurement_quantity_wattnode',
        'voltage_wattnode', 'install_date_wattnode', 'removal_date_wattnode']
    df = df[to_keep]
    df = df.apply(lambda x: x.str.upper() if x.dtype == 'object' else x)
    # relabel identifiers for merging datasets
    df.rename(
        columns={
            'fulcrum_parent_id': 'id1',
            'fulcrum_record_id': 'id0',
            'fulcrum_id': 'id2'},
        inplace=True)
    df.to_hdf(
        root + '/data/interim/fulcrum/ridgeline_wattnodes.h5',
        'df')
    df.to_csv(
        root + '/data/interim/fulcrum/ridgeline_wattnodes.csv',
        index=False)


def preprocess_ridgeline_cts():
    """
    sentient things device attribute data processing
    """
    # load data
    df = pd.read_csv(
        root + '/data/raw/fulcrum/ridgeline_cts.csv',
        dtype=str)
    to_keep = [
        'fulcrum_id', 'fulcrum_parent_id', 'fulcrum_record_id',
        'equipment_label_ct', 'measurement_label_ct',
        'measurement_quantity_ct',
        'amperage_ct', 'install_date_ct', 'removal_date_ct']
    df = df[to_keep]
    df = df.apply(lambda x: x.str.upper() if x.dtype == 'object' else x)
    # relabel identifiers for merging datasets
    df.rename(
        columns={
            'fulcrum_parent_id': 'id1',
            'fulcrum_record_id': 'id0',
            'fulcrum_id': 'id2'},
        inplace=True)
    df.to_hdf(
        root + '/data/interim/fulcrum/ridgeline_cts.h5',
        'df')
    df.to_csv(
        root + '/data/interim/fulcrum/ridgeline_cts.csv',
        index=False)


def preprocess_ridgeline_equipment():
    """
    sentient things device attribute data processing
    """
    # load data
    df = pd.read_csv(
        root + '/data/raw/fulcrum/ridgeline_equipment.csv',
        dtype=str)
    to_keep = [
        'fulcrum_id', 'fulcrum_parent_id', 'fulcrum_record_id',
        'equipment_label', 'equipment_description', 'area_served']
    df = df[to_keep]
    # relabel identifiers for merging datasets
    df.rename(
        columns={
            'fulcrum_parent_id': 'id1',
            'fulcrum_record_id': 'id0',
            'fulcrum_id': 'id2'},
        inplace=True)
    df = df.apply(lambda x: x.str.upper() if x.dtype == 'object' else x)
    df.to_hdf(
        root + '/data/interim/fulcrum/ridgeline_equipment.h5',
        'df')
    df.to_csv(
        root + '/data/interim/fulcrum/ridgeline_equpment.csv',
        index=False)


if __name__ == '__main__':
    unzip_fulcrum_export()
    preprocess_ridgeline()
    preprocess_ridgeline_sentientthings_devices()
    preprocess_ridgeline_sentientthings_devices_measurements()
    preprocess_ridgeline_wattnodes()
    preprocess_ridgeline_cts()
    preprocess_ridgeline_equipment()
