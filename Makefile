setup:
	pip install --user -r requirements.txt
	python src/write_env.py
	python src/write_dirs.py

preprocess:
	python src/preprocess1.py
	python src/preprocess2.py
	python src/process1.py
	python src/process2.py

run:
	python src/util.py
